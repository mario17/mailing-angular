const { resolve,join } = require('path');
const validate  = require('webpack-validator');
const webpack = require('webpack');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const ExtractTextPlugin = require("extract-text-webpack-plugin");


module.exports = (env) => {
	const entry =['webpack-dev-server/client?http://localhost:9000', 'webpack/hot/only-dev-server'];
	const plugins =  [  new webpack.HotModuleReplacementPlugin(), new webpack.NamedModulesPlugin()]
	const publicPath = {  publicPath: '/' }
	const configWebpack = {
		context: resolve('app'),
		entry:{ 
			'app':[ 'babel-polyfill','./app.js' ],
			'vendor': ['angular','angular-ui-router','angular-lazy-image']
		},
		output:{
			path: resolve(__dirname, '../src/public/static/js/'),
			filename: '[name].bundle.js'
		},
		module: {
			rules: [
				{	test: /\.js$/, loader: 'babel-loader', exclude: /(node_modules|bower_components)/ , query:{ presets: [["es2015",{modules: false}],"stage-2"]}	},
				{ 	test: /\.(jpg|png|eot|ttf|woff|svg)$/, loader: 'file-loader',  exclude: /(node_modules|bower_components)/, query:{ name: '[name].[ext]', publicPath: '/static/assets/', outputPath:'../assets/'} },
				{	test: /\.scss$/,	loader: ExtractTextPlugin.extract({ fallback: 'style-loader', use: ['css-loader?importLoaders=1', 'sass-loader']})},
				{	test: /\.css$/, loader: ExtractTextPlugin.extract({ fallback: 'style-loader', use: 'css-loader' })},
				{	test: /\.pug$/, loader: 'pug-loader' }
			]
		},
		devServer: {
			contentBase: join(__dirname, "../src/public/"),
			publicPath: '/',
			compress: true,
			port: 9000,
			hot:  false 
		},
		plugins: [
			new webpack.optimize.CommonsChunkPlugin({name:'vendor', minChunks: 5}),
			new ExtractTextPlugin("styles.css"),
			new HtmlWebpackPlugin({filename:'../../index.html',  template: './templates/empty.html'}),
		],
		resolve: {
			extensions: ['.js','.jpg','png'],
		}
	}
	if (env.dev)  configWebpack.entry.app.push(...entry);
	if (env.dev)  configWebpack.plugins.push(...plugins);
	if (env.dev)  Object.assign(configWebpack.output, publicPath)	
	return  (configWebpack)
}

