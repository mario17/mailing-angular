import angular from 'angular';
import Navbar from './navbar/navbar';
import Search from './search/search';
import Slider from './slider/slider';
import Contact from './contact/contact';




console.log(Contact);
let commonModule = angular.module('app.common', [
  Navbar,
  Slider,
  Search,
  Contact,
])
  
.name;

export default commonModule;