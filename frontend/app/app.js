import 'normalize.css';
import angular from 'angular';
import uiRouter from 'angular-ui-router';
import Common from './01-common/common';
import Pages from './03-pages/pages';
import AppComponent from './app.component';
import configApp from  './app.router'


angular.module('app', [
    uiRouter,
    Common,
    Pages
  ])
  .config(configApp)
  .component('app', AppComponent);


/*import { mainModule } from './constants/index';*/

