configApp.$inject = ['$locationProvider'];

export default  function configApp ($locationProvider) {
	$locationProvider.html5Mode(true).hashPrefix('!');
}
