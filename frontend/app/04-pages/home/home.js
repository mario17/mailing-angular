import angular from 'angular';
import uiRouter from 'angular-ui-router';
import homeComponent from './home.component';
import configHome from './home.router';

console.log(configHome,'configHome');
let homeModule = angular.module('home', [ uiRouter ])
.config(configHome)
.component('home', homeComponent)
.name;

export default homeModule;