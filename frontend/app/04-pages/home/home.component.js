import template from './home.pug';
import controller from './home.controller';
import './home.scss';

let homeComponent = {
  restrict: 'E',
  bindings: {},
  template: template(),
  controller:controller
};

export default homeComponent;