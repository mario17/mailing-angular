configHome.$inject = ['$stateProvider', '$urlRouterProvider'];

export default  function configHome ($stateProvider, $urlRouterProvider) {
	$urlRouterProvider.otherwise('/');
	$stateProvider
		.state('home', {
			url: '/',
			component: 'home'
		}
	);
}
