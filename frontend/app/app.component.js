import template from './app.pug';
import './app.scss';

let appComponent = {
  template: template(),
  restrict: 'E'
};

export default appComponent;