import  template from './navbar.pug';
import controller from './navbar.controller';
import './navbar.scss';

console.log(template());

let navbarComponent = {
  restrict: 'E',
  bindings: {},
  template:template(),
  controller:controller
};

export default navbarComponent;