import  template from './slider.pug';
import controller from './slider.controller';
import './slider.scss';

let sliderComponent = {
  restrict: 'E',
  bindings: {},
  template:template(),
  controller:controller
};

export default sliderComponent;