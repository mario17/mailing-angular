import template from './contact.pug';
import controller from './contact.controller';
import './contact.scss';

let contactComponent = {
  restrict: 'EA',
  bindings: {},
  template:template(),
  controller:controller
};

export default contactComponent;