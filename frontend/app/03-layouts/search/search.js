import angular from 'angular';
import uiRouter from 'angular-ui-router';
import searchComponent from './search.component';
import searchFactory from './search.service';
//import ImageDirective from './search.directive';


let searchModule = angular.module('search', [
	uiRouter
])
//.directive('bnLazySrc', ImageDirective)
.factory('searchFactory', searchFactory)
.component('search', searchComponent)
.name;

export default searchModule;