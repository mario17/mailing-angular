searchFactory.$inject = ['$http','$q'];

function searchFactory ($http,$q){
    console.log('fac')
    const urlBase = 'https://www.googleapis.com/youtube/v3/search?part=snippet&maxResults=10&order=relevance&type=video';
    const APIKEY = '&key=AIzaSyDuZKmHmW3u4R3hHYjdMuhIAMivKqkrmTA';
    const searchFactory = {};

    searchFactory.getVideos = function (q) {
        return $http.get(`${urlBase}${APIKEY}&q=${q}`);
    };
    searchFactory.getAdvertisment = function (id) {
        return $http.get(`${urlBase}/mailings/${id}/ads/`);
    };

    return searchFactory;
}

export default searchFactory;
