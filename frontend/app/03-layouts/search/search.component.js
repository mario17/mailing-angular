import  template from './search.pug';
import controller from './search.controller';
import './search.scss';


let searchComponent = {
  restrict: 'E',
  bindings: {},
  template:template(),
  controller:controller
};

export default searchComponent;