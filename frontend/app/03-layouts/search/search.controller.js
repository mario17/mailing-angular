'use strict';

searchController.$inject = ['$scope','searchFactory'];

export default function searchController($scope, searchFactory) {
	$scope.videos;
	$scope.userSearch = '';
	$scope.getVideos = (val) => {
		console.log(val);
		searchFactory.getVideos($scope.userSearch)
		.then(function (response) {
			console.log(response);
			$scope.videos = response.data.items;
			}, function (error) {
					$scope.status = 'Unable to load customer data: ' + error.message;
		});
	}

	$scope.getNextVideos = () => {
		searchFactory.getVideos($scope.userSearch)
		.then(function (response) {
			console.log(response);
			$scope.videos = response.data.items;
			}, function (error) {
				$scope.status = 'Unable to load customer data: ' + error.message;
		});
	}

/*	$scope.isBoxVisible = true;
	$scope.photos = buildPhotoSet( 100 );
	console.log($scope.photos);
	$scope.changeSource = function() {
		var now = ( new Date() ).getTime();
		for ( var i = 0 ; i < $scope.photos.length ; i++ ) {
			var photo = $scope.photos[ i ];
			photo.src = photo.src.replace( /\d\./i, "1." );
		}

	};

	$scope.clearPhotos = function() {
		$scope.photos = [];
	};

	$scope.hideBox = function() {
		$scope.isBoxVisible = false;
	};

	$scope.rebuildSet = function() {
		$scope.photos = buildPhotoSet( 20 );
	};
	
	function buildPhotoSet( size ) {
		var photos = [];
		var now = ( new Date() ).getTime();
		for ( var i = 0 ; i < size ; i++ ) {
			var index = ( ( i % 3 ) + 1 );
			var version = ( now + i );
			photos.push({
				id: ( i + 1 ),
				src: ( "http://bennadel.github.io/JavaScript-Demos/demos/lazy-src-angularjs/christina-cox-" + index + ".jpg?v=" + version )
			});
	
		}
		return( photos );
	
	}*/
	

}
